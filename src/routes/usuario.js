const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser'); //bodyParser nos permite reicibir parametros por POST
const mysqlConnection = require('../../database.js');
const bcrypt = require('bcrypt');
const { emit } = require('../../database.js');


// create application/json parser
var jsonParser = bodyParser.json();
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });


// Validar usuario
router.get('/login/:email/:password', (req, res) => {
    const { email } = req.params;
    const { password } = req.params;

    mysqlConnection.query('SELECT * FROM usuarios WHERE email = ?', [email], (err, rows, fields) => {
        //almacena la contraseña guardada en la bd
        const hashSaved = rows[0].password;
        //comparacion de contraseñas 
        let compare = bcrypt.compareSync(password, hashSaved);
        if (!err && compare) {
            res.json({ 
                ok: 'Se encontro al usuario ',
                rows: rows
            });
        } else {
            res.json({ 
                ok: 'No se encontro al usuario',
                error: err
            });
        }
    });
});

// GET all usuarios
router.get('/usuarios', (req, res) => {
    mysqlConnection.query('SELECT * FROM usuarios', (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
});


// GET An usuario por Id
router.get('/usuarios/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('SELECT * FROM usuarios WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
});

// DELETE An usuario por id
router.delete('/usuarios/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('DELETE FROM usuarios WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true 
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
});


// create application/json parser
var jsonParser = bodyParser.json();
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// INSERT An usuario
router.post('/usuarios/', urlencodedParser, (req, res) => {
    const { nombre, password, email } = req.body;

    //Encripta la contraseña
    let encriptado = bcrypt.hashSync(password, 9);

    let sql = 'INSERT INTO usuarios(nombre, password, email) VALUES (?,?,?)';
    var valores = [nombre, encriptado, email];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: 'Se agrego con exito el usuario'
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
});


module.exports = router;